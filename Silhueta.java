import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Silhueta {

    public int ocorrencias;
    public int tam_array = 0;
    public int esq;
    public int total = 0;

    public boolean eMaior;
    public ArrayList<Integer> lista = new ArrayList<Integer>();

    public static void main(String[] args){
        Scanner ler = new Scanner(System.in);
        System.out.println("Exemplo de caminho C:/Users/Home/IdeaProjects/Silhueta/src/teste.txt");
        System.out.println("Informe o caminho do arquivo:");
        String caminho = ler.next();


        Silhueta silhueta = new Silhueta();
        silhueta.readFile(caminho);

    }

    public void readFile(String file){
        String caminho = file;


        try {
            int iCont = 1;
            BufferedReader br = new BufferedReader(new FileReader(caminho));
            while(br.ready()){
                String linha = br.readLine();
                if (iCont == 1){
                    ocorrencias = Integer.parseInt(linha);
                }
                else {
                    if (iCont % 2 != 0) {

                        String[] arr = linha.split(" ");
                        ArrayList<Integer> intarray = new ArrayList<>();
                        for (String str : arr) {
                            intarray.add(Integer.parseInt(str));
                        }
                        esq = intarray.get(0);
                        for(int i = 1; i < tam_array; i++){
                            if(intarray.get(i) >= esq){
                                if((lista.isEmpty())){
                                    esq = intarray.get(i);
                                }
                                else{
                                    total += calcSilhueta(lista, esq, intarray.get(i));
                                    lista.clear();
                                    esq = intarray.get(i);
                                }
                            }
                            else {
                                boolean retorno = isContains(intarray.subList(i, intarray.size()-1), intarray.get(i));
                                eMaior = isBigger(intarray.subList(i+1, intarray.size()), intarray.get(i));
                                 if(eMaior && !lista.isEmpty()){
                                    total += calcSilhueta(lista, esq, intarray.get(i));
                                    lista.clear();
                                    esq = intarray.get(i);
                                }
                                if(retorno && !eMaior) {
                                    lista.add(intarray.get(i));
                                }
                                else if(!lista.isEmpty()) {
                                    esq = intarray.get(i);
                                }
                            }
                        }
                        System.out.println("Total: " + total);
                        total = 0;
                        lista.clear();
                    }
                    else {
                        tam_array = Integer.parseInt(linha);
                    }
                }
                iCont += 1;
            }
            br.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public int calcSilhueta(ArrayList<Integer> silhueta, int esq, int dir){
        int total = 0;
        if (dir > esq){
            for(int i = 0; i < silhueta.size(); i++){
                total += esq - silhueta.get(i) ;
            }
        }
        else {
            for(int i = 0; i < silhueta.size(); i++){
                total += dir - silhueta.get(i) ;
            }
        }
        return total;
    }

    public boolean isContains(List<Integer> subSeq, int atual){
        if(subSeq.isEmpty()){
            return false;
        }
        else{
            if (subSeq.get(0) >= atual){
                return true;
            }
            else{
                if(subSeq.size() > 1){
                    return isContains(subSeq.subList(1, subSeq.size()-1), atual);
                }
                else if(subSeq.size() == 1){
                    subSeq.remove(0);
                    return isContains(subSeq, atual);
                }
                else {
                    return isContains(subSeq, atual);
                }
            }
        }
    }

    public boolean isBigger(List<Integer> list, int atual){
        for(Integer elem : list){
            if(elem >= atual){
                return false;
            }
        }
        return true;
    }


}

